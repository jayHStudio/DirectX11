//

#include "GameTimer.h"

#include <windows.h>

GameTimer::GameTimer()
{
	/* ------------------------------------------------------------------------- */
	// 초당 성능 카운터를 얻어와서 역수를 취해 카운터랑 초를 얻어옵니다.

	__int64 countsPerSec;
	QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSec);
	SecondsPerCount = 1.0 / (double)countsPerSec;
}


void GameTimer::Tick()
{
	/* ------------------------------------------------------------------------- */
	// 매프레임마다 경과된 시간 델타타임을 갱신합니다.
	// 게임이 중지상태라면 DeltaTime은 0.0이 됩니다.

	if ( bStopped )
	{
		DeltaTime = 0.0;
		return;
		
	}

	/* ------------------------------------------------------------------------- */
	// (이전 프레임의 시간(카운트) - 현재 프레임의 시간(카운트)) * 카운트당 초
	// 로 경과된 시간(DeltaTime)을 구합니다.

	QueryPerformanceCounter((LARGE_INTEGER*)&CurrentTime);
	DeltaTime = (CurrentTime - PreviousTime) * SecondsPerCount;
	PreviousTime = CurrentTime;

	if ( DeltaTime < 0.0 )
	{
		DeltaTime = 0.0f;
	}
}


float GameTimer::GetTotalTime() const
{
	if ( bStopped )
	{
		/* ------------------------------------------------------------------------- */
		// ((정지되었던 시각 - 정지되었던 시간) - 게임이 시작한 시각) 
		// * 카운트당 시간으로 게임이 실행되고 경과된 시간을 얻어옵니다.	
		// 게임이 정지되었다면 항상 같은 시간이 리턴됩니다.

		return (float)(((StopTime - PausedTime) - BaseTime) * SecondsPerCount);
	}
	else
	{
		/* ------------------------------------------------------------------------- */
		// ((현재 시각 - 정지되었던 시간) - 게임이 시작한 시각) 
		// * 카운트당 시간으로 게임이 실행되고 경과된 시간을 얻어옵니다.	

		return (float)(((CurrentTime - PausedTime) - BaseTime) * SecondsPerCount);
	}
}


float GameTimer::GetDeltaTime() const
{
	return (float)DeltaTime;
}


void GameTimer::Reset()
{
	__int64 CurrentTime;
	QueryPerformanceCounter((LARGE_INTEGER*)&CurrentTime);

	this->CurrentTime = CurrentTime;
	BaseTime = CurrentTime;
	PreviousTime = CurrentTime;
	StopTime = 0;
	bStopped = false;
}


void GameTimer::Stop()
{
	/* ------------------------------------------------------------------------- */
	// 중지된 현재 시간을 갱신하고 게임중지 상태로 전환합니다.

	if ( bStopped == false )
	{
		QueryPerformanceCounter((LARGE_INTEGER*)&CurrentTime);
		StopTime = CurrentTime;
		bStopped = true;
	}
}


void GameTimer::Start()
{
	/* ------------------------------------------------------------------------- */
	// 게임을 다시 재개합니다.
	// 중지된시간과 재개된 시간의 델타값을 구해서 정지되있던 시간을 누적합니다.

	__int64 StartTime;
	QueryPerformanceCounter((LARGE_INTEGER*)&StartTime);

	if ( bStopped )
	{
		PausedTime += (StartTime - StopTime);

		PreviousTime = StartTime;
		StopTime = 0;
		bStopped = false;
	}
}

