#pragma once

#include <windows.h>
#include <DirectXMath.h>
#include <DirectXPackedVector.h>

#include "dxerr.h"


/* ------------------------------------------------------------------------- */
// 간단한 에러체크를 시행합니다.

#if defined(DEBUG) || defined(_DEBUG)
#ifndef HR
#define HR(x)														\
{																	\
	HRESULT hResult = (x);											\
																	\
	if ( FAILED(hResult) )											\
	{																\
		DXTrace(__FILEW__, (DWORD)__LINE__, hResult, L#x, true);	\
	}																\
}
#endif
#else
#ifndef HR
#define HR(x) (x)
#endif
#endif

/* ------------------------------------------------------------------------- */
// COM 인터페이스를 해제한다.

#define RELEASECOM(x) { if(x) { x->Release(); x = 0; } }


// #define XMGLOBALCONST extern CONST __declspec(selectany)
//   1. extern so there is only one copy of the variable, and not a separate
//      private copy in each .obj.
//   2. __declspec(selectany) so that the compiler does not complain about
//      multiple definitions in a .cpp file (it can pick anyone and discard 
//      the rest because they are constant--all the same).

namespace Colors
{
	XMGLOBALCONST DirectX::XMVECTORF32 White ={ 1.0f, 1.0f, 1.0f, 1.0f };
	XMGLOBALCONST DirectX::XMVECTORF32 Black = { 0.0f, 0.0f, 0.0f, 1.0f };
	XMGLOBALCONST DirectX::XMVECTORF32 Red = { 1.0f, 0.0f, 0.0f, 1.0f };
	XMGLOBALCONST DirectX::XMVECTORF32 Green = { 0.0f, 1.0f, 0.0f, 1.0f };
	XMGLOBALCONST DirectX::XMVECTORF32 Blue = { 0.0f, 0.0f, 1.0f, 1.0f };
	XMGLOBALCONST DirectX::XMVECTORF32 Yellow = { 1.0f, 1.0f, 0.0f, 1.0f };
	XMGLOBALCONST DirectX::XMVECTORF32 Cyan = { 0.0f, 1.0f, 1.0f, 1.0f };
	XMGLOBALCONST DirectX::XMVECTORF32 Magenta = { 1.0f, 0.0f, 1.0f, 1.0f };
	XMGLOBALCONST DirectX::XMVECTORF32 Silver = { 0.75f, 0.75f, 0.75f, 1.0f };
	XMGLOBALCONST DirectX::XMVECTORF32 LightSteelBlue = { 0.69f, 0.77f, 0.87f, 1.0f };
}