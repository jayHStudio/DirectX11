// JayH

#pragma once
#pragma comment(lib, "d3d11.lib")
#if defined(DEBUG) || defined(_DEBUG)
#pragma comment(lib, "Effects11d.lib")
#else
#pragma comment(lib, "Effects11.lib")
#endif

#include "GameTimer.h"

#include <windows.h>
#include <string>
#include <d3d11.h>

class Direct3DApplication
{
	friend int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd);
	friend LRESULT CALLBACK MainWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	
public:
	Direct3DApplication(HINSTANCE hInstance);
	virtual ~Direct3DApplication();

	float GetAspectRatio() const;
	
protected:
	virtual bool Initialize();
	virtual void UpdateScene(float DeltaTime) = 0;
	virtual void DrawScene() = 0;
			

	virtual LRESULT MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	
	virtual void OnMouseDown(WPARAM ButtonState, int x, int y) {}
	virtual void OnMouseUp(WPARAM ButtonState, int x, int y) {}
	virtual void OnMouseMove(WPARAM ButtonState, int x, int y) {}

	virtual void OnResize();

private:
	bool InitializeMainWindow();
	bool InitializeDirect3D();
	void CalculateFrameStats();

	int Run();

protected:
	/* ------------------------------------------------------------------------- */
	// 응용 프로그램, 윈도우

	HINSTANCE hAppInstance;
	HWND hMainWindow = 0;

	std::wstring MainWindowCaption = L"Direct3D11 Application";
	int ClientWidth = 800;
	int ClientHeight = 600;
	
	/* ------------------------------------------------------------------------- */
	// 다이렉트 3D

	ID3D11Device* D3DDevice = nullptr;
	ID3D11DeviceContext* D3DImmediateContext = nullptr;
	IDXGISwapChain* SwapChain = nullptr;
	ID3D11Texture2D* DepthStencilBuffer = nullptr;
	ID3D11RenderTargetView* RenderTargetView = nullptr;
	ID3D11DepthStencilView* DepthStencilView = nullptr;
	D3D_DRIVER_TYPE D3DDriverType = D3D_DRIVER_TYPE_HARDWARE;
	D3D11_VIEWPORT ScreenViewport;

	UINT MsaaQuality = 0;

	/* ------------------------------------------------------------------------- */
	// 프레임

	GameTimer FrameTimer;

	/* ------------------------------------------------------------------------- */
	// 상태 변수

	bool bAppPaused = false;
	bool bMinimized = false;
	bool bMaximized = false;
	bool bResizing = false;
	bool bEnable4xMsaa = true;

};