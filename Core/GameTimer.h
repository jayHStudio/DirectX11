//

#pragma once


class GameTimer final
{
public:

	GameTimer();

	void Tick();
	void Stop();
	void Start();
	void Reset();

	float GetTotalTime() const;
	float GetDeltaTime() const;

private:

	double SecondsPerCount = 0.0;
	double DeltaTime = -1.0;

	__int64 BaseTime = 0;
	__int64 PausedTime = 0;
	__int64 StopTime = 0;
	__int64 PreviousTime = 0;
	__int64 CurrentTime = 0;

	bool bStopped = false;
};