//

#include "Direct3DApplication.h"
#include "Direct3DUtil.h"

#include <WindowsX.h>
#include <sstream>
#include <assert.h>


static Direct3DApplication* gDirect3dApplication = nullptr;

LRESULT CALLBACK MainWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	assert(gDirect3dApplication);
	return gDirect3dApplication->MsgProc(hWnd, msg, wParam, lParam);
}


LRESULT Direct3DApplication::MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch ( msg )
	{
		case WM_ACTIVATE:
		{
			if ( LOWORD(wParam) == WA_INACTIVE )
			{
				bAppPaused = true;
				FrameTimer.Stop();
			}
			else
			{
				bAppPaused = false;
				FrameTimer.Start();
			}

			return 0;
		}

		case WM_SIZE:
		{
			ClientWidth = LOWORD(lParam);
			ClientHeight = HIWORD(lParam);

			if ( D3DDevice )
			{
				if ( wParam == SIZE_MINIMIZED )
				{
					bAppPaused = true;
					bMinimized = true;
					bMaximized = false;
				}
				else if ( wParam == SIZE_MAXIMIZED )
				{
					bAppPaused = false;
					bMinimized = false;
					bMaximized = true;
					OnResize();
				}
				else if ( wParam == SIZE_RESTORED )
				{
					if ( bMinimized )
					{
						bAppPaused = false;
						bMinimized = false;
						OnResize();
					}
					else if ( bMaximized )
					{
						bAppPaused = false;
						bMaximized = false;
						OnResize();
					}
					else if ( bResizing )
					{

					}
					else
					{
						OnResize();
					}
				}
			}

			return 0;
		}

		case WM_ENTERSIZEMOVE:
		{
			bAppPaused = true;
			bResizing = true;
			FrameTimer.Stop();

			return 0;
		}

		case WM_EXITSIZEMOVE:
		{
			bAppPaused = false;
			bResizing = false;
			FrameTimer.Start();
			OnResize();

			return 0;
		}

		case WM_DESTROY:
		{
			PostQuitMessage(0);

			return 0;
		}

		case WM_MENUCHAR:
		{
			return MAKELRESULT(0, MNC_CLOSE);
		}

		case WM_GETMINMAXINFO:
		{
			((MINMAXINFO*)lParam)->ptMinTrackSize.x = 200;
			((MINMAXINFO*)lParam)->ptMinTrackSize.y = 200;

			return 0;
		}

		case WM_LBUTTONDOWN:
		case WM_MBUTTONDOWN:
		case WM_RBUTTONDOWN:
		{
			OnMouseDown(wParam, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));

			return 0;
		}
		case WM_LBUTTONUP:
		case WM_MBUTTONUP:
		case WM_RBUTTONUP:
		{
			OnMouseUp(wParam, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));

			return 0;
		}

		case WM_MOUSEMOVE:
		{
			OnMouseMove(wParam, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));

			return 0;
		}
	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}


Direct3DApplication::Direct3DApplication(HINSTANCE hInstance)
	: hAppInstance(hInstance)
{
	ZeroMemory(&ScreenViewport, sizeof(D3D11_VIEWPORT));
	assert(gDirect3dApplication == nullptr);
	gDirect3dApplication = this;
}


Direct3DApplication::~Direct3DApplication()
{
	RELEASECOM(RenderTargetView);
	RELEASECOM(DepthStencilView);
	RELEASECOM(SwapChain);
	RELEASECOM(DepthStencilBuffer);

	if ( D3DImmediateContext )
	{
		D3DImmediateContext->ClearState();
	}

	RELEASECOM(D3DImmediateContext);
	RELEASECOM(D3DDevice);
}


float Direct3DApplication::GetAspectRatio() const
{
	return static_cast<float>(ClientWidth) / ClientHeight;
}


bool Direct3DApplication::Initialize()
{
	if ( InitializeMainWindow() == false || InitializeDirect3D() == false )
	{
		return false;
	}

	return true;
}


bool Direct3DApplication::InitializeMainWindow()
{
	/* ------------------------------------------------------------------------- */
	// 윈도우 클래스를 서술하는 구조체 할당

	WNDCLASS NewWindowClass;
	NewWindowClass.style = CS_HREDRAW | CS_VREDRAW;
	NewWindowClass.lpfnWndProc = MainWndProc;
	NewWindowClass.cbClsExtra = 0;
	NewWindowClass.cbWndExtra = 0;
	NewWindowClass.hInstance = hAppInstance;
	NewWindowClass.hIcon = LoadIcon(0, IDI_APPLICATION);
	NewWindowClass.hCursor = LoadCursor(0, IDC_ARROW);
	NewWindowClass.hbrBackground = (HBRUSH)GetStockObject(NULL_BRUSH);
	NewWindowClass.lpszMenuName = 0;
	NewWindowClass.lpszClassName = L"D3DWindowClassName";

	/* ------------------------------------------------------------------------- */
	// 윈도우 클래스를 등록

	if ( !RegisterClass(&NewWindowClass) )
	{
		MessageBox(0, L"RegisterClass Failed.", nullptr, 0);
		return false;
	}

	/* ------------------------------------------------------------------------- */
	// 클라이언트 영역에 윈도우 사이즈를 재조정하고 윈도우를 생성

	RECT WindowRect = { 0, 0, ClientWidth, ClientHeight };
	AdjustWindowRect(&WindowRect, WS_OVERLAPPEDWINDOW, false);
	int Width = WindowRect.right - WindowRect.left;
	int Height = WindowRect.bottom - WindowRect.top;

	hMainWindow = CreateWindow(L"D3DWindowClassName", MainWindowCaption.c_str(), WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, Width, Height, 0, 0, hAppInstance, nullptr);

	if ( hMainWindow == INVALID_HANDLE_VALUE )
	{
		MessageBox(0, L"CreateWindow Failed.", nullptr, 0);
		return false;
	}

	/* ------------------------------------------------------------------------- */
	// 윈도우를 띄우고 업데이트

	ShowWindow(hMainWindow, SW_SHOW);
	UpdateWindow(hMainWindow);

	return true;
}


bool Direct3DApplication::InitializeDirect3D()
{
	/* ------------------------------------------------------------------------- */
	// 다이렉트 3D 장치 생성

	UINT CreateDeviceFlags = 0;
#if defined(DEBUG) || defined(_DEBUG)
	CreateDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_FEATURE_LEVEL FeatureLevel;
	HRESULT hResult = D3D11CreateDevice(
		0,
		D3DDriverType,
		0,
		CreateDeviceFlags,
		nullptr,
		0,
		D3D11_SDK_VERSION,
		&D3DDevice,
		&FeatureLevel,
		&D3DImmediateContext);

	if ( FAILED(hResult) )
	{
		MessageBox(0, L"D3D11CreateDevice Failed.", nullptr, 0);
	}

	if ( FeatureLevel != D3D_FEATURE_LEVEL_11_0 )
	{
		MessageBox(0, L"Direct3D Feature Level 11 unsupported.", nullptr, 0);
	}

	/* ------------------------------------------------------------------------- */
	// 렌더 타겟이 되는 버퍼와 픽셀당 부분픽셀을 4개를 샘플링하는 기준으로
	// 현재 지원하는 다중표본화의 퀄리티의 개수를 해당 하드웨어에 질의합니다.
	// 모든 Direct3D 11 지원 하드웨어 장치는 MSAA를 지원하기 때문에
	// MsaaQuality는 반드시 1개 이상이어야 합니다.

	HR(D3DDevice->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, 4, &MsaaQuality));
	assert(MsaaQuality > 0);

	/* ------------------------------------------------------------------------- */
	// 스왑체인을 생성하기위한 구조체를 채운다.

	DXGI_SWAP_CHAIN_DESC SwapChainDesc;
	SwapChainDesc.BufferDesc.Width = ClientWidth;
	SwapChainDesc.BufferDesc.Height = ClientHeight;
	SwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	SwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	SwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	SwapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	SwapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	if ( bEnable4xMsaa )
	{
		SwapChainDesc.SampleDesc.Count = 4;
		SwapChainDesc.SampleDesc.Quality = MsaaQuality - 1;
	}
	else
	{
		SwapChainDesc.SampleDesc.Count = 1;
		SwapChainDesc.SampleDesc.Quality = 0;
	}

	SwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	SwapChainDesc.BufferCount = 1;
	SwapChainDesc.OutputWindow = hMainWindow;
	SwapChainDesc.Windowed = true;
	SwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	SwapChainDesc.Flags = 0;

	/* ------------------------------------------------------------------------- */
	// 서술한 스왑체인의 구조체로 스왑체인을 생성한다.

	IDXGIDevice* DXGIDevice = nullptr;
	HR(D3DDevice->QueryInterface(__uuidof(IDXGIDevice), (void**)&DXGIDevice));
	IDXGIAdapter* DXGIAdapter = nullptr;
	HR(DXGIDevice->GetParent(__uuidof(IDXGIAdapter), (void**)&DXGIAdapter));
	IDXGIFactory* DXGIFactory = nullptr;
	HR(DXGIAdapter->GetParent(__uuidof(IDXGIFactory), (void**)&DXGIFactory));

	HR(DXGIFactory->CreateSwapChain(D3DDevice, &SwapChainDesc, &SwapChain));

	RELEASECOM(DXGIDevice);
	RELEASECOM(DXGIAdapter);
	RELEASECOM(DXGIDevice);

	/* ------------------------------------------------------------------------- */
	// 깊이 스텐실 버퍼를 생성하고 랜더 타겟, 깊이 / 스텐실 자원 뷰를
	// 출력 병합기에 바인딩합니다.

	OnResize();

	return true;
}


void Direct3DApplication::CalculateFrameStats()
{
	static int FrameCount = 0;
	static float TimeElapsed = 0.0f;

	FrameCount++;

	if ( (FrameTimer.GetTotalTime() - TimeElapsed) >= 1.0f )
	{
		float FPS = (float)FrameCount;
		float MilliSecondsPerFrame = 1000.0f / FPS;

		std::wostringstream OutStream;
		OutStream.precision(6);

		OutStream << MainWindowCaption << L"   " << L"FPS : " << FPS << L"  Frame Time : " << MilliSecondsPerFrame << L" (ms)";

		SetWindowText(hMainWindow, OutStream.str().c_str());

		FrameCount = 0;
		TimeElapsed += 1.0f;
	}
}


void Direct3DApplication::OnResize()
{
	/* ------------------------------------------------------------------------- */
	// 사용자로부터 리사이즈 메시지를 받을때마다 호출됩니다.

	assert(D3DImmediateContext);
	assert(D3DDevice);
	assert(SwapChain);

	/* ------------------------------------------------------------------------- */
	// 윈도우의 창이 리사이즈되면 후면버퍼의 크기도 변경이 된다는 의미이므로
	// 후면버퍼의 사이즈를 재조정하고 자원뷰들을 해제하고 다시 생성합니다.

	RELEASECOM(RenderTargetView);
	RELEASECOM(DepthStencilView);
	RELEASECOM(DepthStencilBuffer);

	/* ------------------------------------------------------------------------- */
	// [GetBuffer] 첫번째 인자는 얻고자 하는 후면 버퍼의 색인을 의미합니다.
	// 교환 사슬에서 ID3D11Texture2D 클래스의 고유식별자를 찾아
	// 해당하는 타입의 객체의 포인터를 반환합니다.
	// [CreateRenderTargetView] 두번째 인자는 자료 형식을 서술합니다.
	// 현재 랜더타겟을 후면버퍼로 할것이고 후면버퍼는 무형식이 아니라
	// 만들어질때 형식이 정혀져있으므로 nullptr를 전달하면 해당하는 형식을
	// 사용하게 됩니다.

	HR(SwapChain->ResizeBuffers(1, ClientWidth, ClientHeight, DXGI_FORMAT_R8G8B8A8_UNORM, 0));
	ID3D11Texture2D* BackBuffer = nullptr;
	HR(SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&BackBuffer)));
	HR(D3DDevice->CreateRenderTargetView(BackBuffer, nullptr, &RenderTargetView));
	RELEASECOM(BackBuffer);

	/* ------------------------------------------------------------------------- */
	// 깊이 스텐실 버퍼를 재생성 합니다.

	D3D11_TEXTURE2D_DESC DepthStencilDesc;
	DepthStencilDesc.Width = ClientWidth;
	DepthStencilDesc.Height = ClientHeight;
	DepthStencilDesc.MipLevels = 1;
	DepthStencilDesc.ArraySize = 1;
	DepthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;

	if ( bEnable4xMsaa )
	{
		DepthStencilDesc.SampleDesc.Count = 4;
		DepthStencilDesc.SampleDesc.Quality = MsaaQuality - 1;
	}
	else
	{
		DepthStencilDesc.SampleDesc.Count = 1;
		DepthStencilDesc.SampleDesc.Quality = 0;
	}

	DepthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
	DepthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	DepthStencilDesc.CPUAccessFlags = 0;
	DepthStencilDesc.MiscFlags = 0;

	HR(D3DDevice->CreateTexture2D(&DepthStencilDesc, nullptr, &DepthStencilBuffer));
	HR(D3DDevice->CreateDepthStencilView(DepthStencilBuffer, nullptr, &DepthStencilView));

	/* ------------------------------------------------------------------------- */
	// 랜더 타겟 뷰와 깊이 / 스텐실 자원 뷰를 출력 병합기에 바인딩 합니다.

	D3DImmediateContext->OMSetRenderTargets(1, &RenderTargetView, DepthStencilView);

	/* ------------------------------------------------------------------------- */
	// 뷰포트의 트랜스폼을 변경합니다.

	ScreenViewport.TopLeftX = 0;
	ScreenViewport.TopLeftY = 0;
	ScreenViewport.Width = static_cast<float>(ClientWidth);
	ScreenViewport.Height = static_cast<float>(ClientHeight);
	ScreenViewport.MinDepth = 0.0f;
	ScreenViewport.MaxDepth = 1.0f;

	D3DImmediateContext->RSSetViewports(1, &ScreenViewport);
}


int Direct3DApplication::Run()
{
	/* ------------------------------------------------------------------------- */
	// 게임 루프를 돌립니다.

	MSG msg = { 0 };
	FrameTimer.Reset();

	while ( msg.message != WM_QUIT )
	{
		/* ------------------------------------------------------------------------- */
		// 사용자로부터 메시지가 전달되면 메시지를 해석하고 처리합니다.

		if ( PeekMessage(&msg, 0, 0, 0, PM_REMOVE) )
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		/* ------------------------------------------------------------------------- */
		// 메시지가 없다면 게임루프를 돌립니다.

		else
		{
			FrameTimer.Tick();

			if ( bAppPaused == false )
			{
				CalculateFrameStats();
				UpdateScene(FrameTimer.GetDeltaTime());
				DrawScene();
			}
			else
			{
				Sleep(100);
			}
		}
	}

	return (int)msg.wParam;
}
