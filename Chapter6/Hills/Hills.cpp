//

#include <windows.h>
#include <crtdbg.h>
#include <assert.h>
#include <d3dcompiler.h>
#include <locale.h>
#include <fstream>

#include "Direct3DApplication.h"
#include "GeometryGenerator.h"
#include "Direct3DUtil.h"
#include "MathHelper.h"
#include "d3dx11effect.h"


using namespace DirectX;
using namespace DirectX::PackedVector;

struct MyVertex
{
	XMFLOAT3 Position;
	XMFLOAT4 Color;
};


class Hills : public Direct3DApplication
{
public:
	Hills(HINSTANCE hInstance);
	virtual ~Hills();

	virtual bool Initialize() override;
	void UpdateScene(float DeltaTime) override;
	void DrawScene() override;
	void OnResize() override;

	void OnMouseDown(WPARAM ButtonState, int x, int y) override;
	void OnMouseUp(WPARAM ButtonState, int x, int y) override;
	void OnMouseMove(WPARAM ButtonState, int x, int y) override;

private:
	void BuildGeometryBuffers();
	void BuildFX();
	void BuildVertexLayout();

	float GetHeight(float x, float z) const;

private:
	ID3D11Buffer* VB = nullptr;
	ID3D11Buffer* IB = nullptr;

	ID3DX11Effect* FX = nullptr;
	ID3DX11EffectTechnique* Tech = nullptr;
	ID3DX11EffectMatrixVariable* FXWorldViewProjMatrix = nullptr;

	ID3D11InputLayout* InputLayout = nullptr;

	XMFLOAT4X4 GridWorldMatrix;
	UINT mGridIndexCount = 0;

	XMFLOAT4X4 ViewMatrix;
	XMFLOAT4X4 ProjectionMatrix;

	float Theta = 1.5f * MathHelper::Pi;
	float Phi = 0.1f * MathHelper::Pi;
	float Radius = 200.0f;

	POINT LastMousePosition;

};


Hills::Hills(HINSTANCE hInstance)
	: Direct3DApplication(hInstance)
{
	MainWindowCaption = L"Hills Demo";

	LastMousePosition.x = 0;
	LastMousePosition.y = 0;

	XMMATRIX I = XMMatrixIdentity();
	XMStoreFloat4x4(&GridWorldMatrix, I);
	XMStoreFloat4x4(&ViewMatrix, I);
	XMStoreFloat4x4(&ProjectionMatrix, I);
}


Hills::~Hills()
{
	RELEASECOM(VB);
	RELEASECOM(IB);
	RELEASECOM(FX);
	RELEASECOM(InputLayout);
}


bool Hills::Initialize()
{
	if ( Direct3DApplication::Initialize() == false )
	{
		return false;
	}

	BuildGeometryBuffers();
	BuildFX();
	BuildVertexLayout();

	return true;
}


void Hills::UpdateScene(float DeltaTime)
{
	// 카메라의 위치를 구면 이동한다.

	float x = Radius * sinf(Phi) * cosf(Theta);
	float z = Radius * sinf(Phi) * sinf(Theta);
	float y = Radius * cosf(Phi);

	// 시야 행렬을 새로 갱신한다.

	XMVECTOR CameraPosition = XMVectorSet(x, y, z, 1.0f);
	XMVECTOR LootAt = XMVectorZero();
	XMVECTOR Up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	XMMATRIX ViewMatrix = XMMatrixLookAtLH(CameraPosition, LootAt, Up);

	XMStoreFloat4x4(&this->ViewMatrix, ViewMatrix);
}


void Hills::DrawScene()
{
	// 후면 버퍼를 지운다.
	D3DImmediateContext->ClearRenderTargetView(RenderTargetView, reinterpret_cast<const float*>(&Colors::LightSteelBlue));

	// 깊이 스텐실 버퍼의 깊이를 1.0f으로 초기화한다.
	D3DImmediateContext->ClearDepthStencilView(DepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	// 입력배치를 입력조립기에 바인드한다.
	D3DImmediateContext->IASetInputLayout(InputLayout);

	// 입력조립의 방식을 삼각형 목록으로 설정한다.
	D3DImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	UINT Stride = sizeof(MyVertex);
	UINT Offset = 0;

	// 정점버퍼를 입력조립기에 바인딩한다.
	D3DImmediateContext->IASetVertexBuffers(0, 1, &VB, &Stride, &Offset);

	// 인덱스버퍼를 입력조립기에 바인딩한다.
	D3DImmediateContext->IASetIndexBuffer(IB, DXGI_FORMAT_R32_UINT, 0);

	XMMATRIX WorldMatrix = XMLoadFloat4x4(&this->GridWorldMatrix);
	XMMATRIX ViewMatrix = XMLoadFloat4x4(&this->ViewMatrix);
	XMMATRIX ProjectionMatrix = XMLoadFloat4x4(&this->ProjectionMatrix);
	XMMATRIX WorldViewProjMatrix = WorldMatrix * ViewMatrix * ProjectionMatrix;

	D3DX11_TECHNIQUE_DESC techDesc;
	Tech->GetDesc(&techDesc);
	for ( UINT p = 0; p < techDesc.Passes; ++p )
	{
		FXWorldViewProjMatrix->SetMatrix(reinterpret_cast<float*>(&WorldViewProjMatrix));
		Tech->GetPassByIndex(p)->Apply(0, D3DImmediateContext);
		D3DImmediateContext->DrawIndexed(mGridIndexCount, 0, 0);
	}

	HR(SwapChain->Present(0, 0));
}


void Hills::OnResize()
{
	Direct3DApplication::OnResize();

	// 윈도우가 리사이징되면 종횡비를 다시 갱신하고 새 투영행렬을 만들어야한다.
	XMMATRIX ProjectionMatrix = XMMatrixPerspectiveFovLH(0.25f * MathHelper::Pi, GetAspectRatio(), 1.0f, 1000.0f);
	XMStoreFloat4x4(&this->ProjectionMatrix, ProjectionMatrix);
}


void Hills::OnMouseDown(WPARAM ButtonState, int x, int y)
{
	LastMousePosition.x = x;
	LastMousePosition.y = y;

	SetCapture(hMainWindow);
}


void Hills::OnMouseUp(WPARAM ButtonState, int x, int y)
{
	ReleaseCapture();
}


void Hills::OnMouseMove(WPARAM ButtonState, int x, int y)
{
	if ( (ButtonState & MK_LBUTTON) != 0 )
	{
		float DeltaX = XMConvertToRadians(0.25f * static_cast<float>(x - LastMousePosition.x));
		float DeltaY = XMConvertToRadians(0.25f * static_cast<float>(y - LastMousePosition.y));

		Theta += DeltaX;
		Phi += DeltaY;

		Phi = MathHelper::Clamp(Phi, 0.1f, MathHelper::Pi - 0.1f);
	}
	else if ( (ButtonState & MK_RBUTTON) != 0 )
	{
		float DeltaX = 0.2f * static_cast<float>(x - LastMousePosition.x);
		float DeltaY = 0.2f * static_cast<float>(y - LastMousePosition.y);

		Radius += DeltaY - DeltaY;

		Radius = MathHelper::Clamp(Radius, 50.0f, 500.f);
	}

	LastMousePosition.x = x;
	LastMousePosition.y = y;
}


void Hills::BuildGeometryBuffers()
{
	GeometryGenerator::MeshData grid;
	GeometryGenerator geoGen;

	geoGen.CreateGrid(160.0f, 160.0f, 50, 50, grid);

	mGridIndexCount = grid.Indices.size();
	std::vector<MyVertex> vertices(grid.Vertices.size());

	// 그리드 메시의 각 정점마다 루프
	for ( size_t i = 0; i < grid.Vertices.size(); ++i )
	{
		// 정점의 위치를 가져옴
		XMFLOAT3 p = grid.Vertices[i].Position;

		// 높이 함수를 적용해서 y의 위치 변경
		p.y = GetHeight(p.x, p.z);

		// 변경된 정점의 위치를 새 백터에 저장
		vertices[i].Position = p;

		// Color the vertex based on its height.
		if ( p.y < -10.0f )
		{
			// Sandy beach color.
			vertices[i].Color = XMFLOAT4(1.0f, 0.96f, 0.62f, 1.0f);
		}
		else if ( p.y < 5.0f )
		{
			// Light yellow-green.
			vertices[i].Color = XMFLOAT4(0.48f, 0.77f, 0.46f, 1.0f);
		}
		else if ( p.y < 12.0f )
		{
			// Dark yellow-green.
			vertices[i].Color = XMFLOAT4(0.1f, 0.48f, 0.19f, 1.0f);
		}
		else if ( p.y < 20.0f )
		{
			// Dark brown.
			vertices[i].Color = XMFLOAT4(0.45f, 0.39f, 0.34f, 1.0f);
		}
		else
		{
			// White snow.
			vertices[i].Color = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
		}
	}

	D3D11_BUFFER_DESC vbd;
	vbd.Usage = D3D11_USAGE_IMMUTABLE;
	vbd.ByteWidth = sizeof(MyVertex) * grid.Vertices.size();
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.CPUAccessFlags = 0;
	vbd.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA vinitData;
	vinitData.pSysMem = &vertices[0];
	HR(D3DDevice->CreateBuffer(&vbd, &vinitData, &VB));

	D3D11_BUFFER_DESC ibd;
	ibd.Usage = D3D11_USAGE_IMMUTABLE;
	ibd.ByteWidth = sizeof(UINT) * mGridIndexCount;
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibd.CPUAccessFlags = 0;
	ibd.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA iinitData;
	iinitData.pSysMem = &grid.Indices[0];
	HR(D3DDevice->CreateBuffer(&ibd, &iinitData, &IB));
}


void Hills::BuildFX()
{
	// 입력 파일스트림 오픈
	std::ifstream fin("FX/Color.fxo", std::ios::binary);

	fin.seekg(0, std::ios_base::end);
	int size = (int)fin.tellg();
	fin.seekg(0, std::ios_base::beg);
	std::vector<char> compiledShader(size);

	// fin 객체를 읽고 compiledShader 를 채움
	fin.read(&compiledShader[0], size);
	fin.close();

	// 효과 객체를 생성
	HR(D3DX11CreateEffectFromMemory(&compiledShader[0], size, 0, D3DDevice, &FX));

	Tech = FX->GetTechniqueByName("ColorTech");
	FXWorldViewProjMatrix = FX->GetVariableByName("gWorldViewProjMatrix")->AsMatrix();

}


void Hills::BuildVertexLayout()
{
	D3D11_INPUT_ELEMENT_DESC VertexDesc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	D3DX11_PASS_DESC PassDesc;
	Tech->GetPassByIndex(0)->GetDesc(&PassDesc);
	HR(D3DDevice->CreateInputLayout(VertexDesc, 2, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &InputLayout));
}


float Hills::GetHeight(float x, float z) const
{
	return 0.3f * (z * sinf(0.1f * x) + x * cosf(0.1f * z));
}


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd)
{
	/* ------------------------------------------------------------------------- */
	// 메모리 누수 체크

#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	Hills theApp(hInstance);

	if ( theApp.Initialize() == false )
	{
		return 0;
	}

	return theApp.Run();
}