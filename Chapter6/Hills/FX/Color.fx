

cbuffer cbPerObject
{
	float4x4 gWorldViewProjMatrix;
};


struct VertexIn
{
	float3 PosL		: POSITION;
	float4 Color	: COLOR;
};


struct VertexOut
{
	float4 PosH		: SV_POSITION;
	float4 Color	: COLOR;
};


VertexOut VS(VertexIn vIn)
{
	VertexOut vOut;

	vOut.PosH = mul(float4(vIn.PosL, 1.0f), gWorldViewProjMatrix);

	vOut.Color = vIn.Color;

	return vOut;
}


float4 PS(VertexOut pIn) : SV_Target
{
	return pIn.Color;
}


technique11 ColorTech
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_5_0, PS()));
	}
}