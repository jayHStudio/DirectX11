//

#include <windows.h>
#include <crtdbg.h>
#include <assert.h>
#include <d3dcompiler.h>
#include <locale.h>

#include "Direct3DApplication.h"
#include "Direct3DUtil.h"
#include "MathHelper.h"
#include "d3dx11effect.h"


using namespace DirectX;
using namespace DirectX::PackedVector;

struct MyVertex
{
	XMFLOAT3 Position;
	XMFLOAT4 Color;
};


class Box : public Direct3DApplication
{

public:

	Box(HINSTANCE hInstance);
	virtual ~Box();

	virtual bool Initialize() override;
	void UpdateScene(float DeltaTime) override;
	void DrawScene() override;
	void OnResize() override;

	void OnMouseDown(WPARAM ButtonState, int x, int y) override;
	void OnMouseUp(WPARAM ButtonState, int x, int y) override;
	void OnMouseMove(WPARAM ButtonState, int x, int y) override;

private:

	void BuildGeometryBuffers();
	void BuildFX();
	void BuildVertexLayout();

private:

	ID3D11Buffer* BoxVB = nullptr;
	ID3D11Buffer* BoxIB = nullptr;

	ID3DX11Effect* FX = nullptr;
	ID3DX11EffectTechnique* Tech = nullptr;
	ID3DX11EffectMatrixVariable* FXWorldViewProjMatrix = nullptr;

	ID3D11InputLayout* InputLayout = nullptr;

	XMFLOAT4X4 WorldMatrix;
	XMFLOAT4X4 ViewMatrix;
	XMFLOAT4X4 ProjectionMatrix;

	float Theta = 1.5f * MathHelper::PI;
	float Phi = 0.25f * MathHelper::PI;
	float Radius = 5.0f;

	POINT LastMousePosition;

};


Box::Box(HINSTANCE hInstance)
	: Direct3DApplication(hInstance)
{
	MainWindowCaption = L"Box Demo";

	LastMousePosition.x = 0;
	LastMousePosition.y = 0;

	XMMATRIX I = XMMatrixIdentity();
	XMStoreFloat4x4(&WorldMatrix, I);
	XMStoreFloat4x4(&ViewMatrix, I);
	XMStoreFloat4x4(&ProjectionMatrix, I);
}


Box::~Box()
{
	RELEASECOM(BoxVB);
	RELEASECOM(BoxIB);
	RELEASECOM(FX);
	RELEASECOM(InputLayout);
}


bool Box::Initialize()
{
	if ( Direct3DApplication::Initialize() == false )
	{
		return false;
	}

	BuildGeometryBuffers();
	BuildFX();
	BuildVertexLayout();

	return true;
}


void Box::UpdateScene(float DeltaTime)
{
	// 카메라의 위치를 구면 이동한다.

	float x = Radius * sinf(Phi) * cosf(Theta);
	float z = Radius * sinf(Phi) * sinf(Theta);
	float y = Radius * cosf(Phi);

	// 시야 행렬을 새로 갱신한다.

	XMVECTOR CameraPosition = XMVectorSet(x, y, z, 1.0f);
	XMVECTOR LootAt = XMVectorZero();
	XMVECTOR Up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	XMMATRIX ViewMatrix = XMMatrixLookAtLH(CameraPosition, LootAt, Up);

	XMStoreFloat4x4(&this->ViewMatrix, ViewMatrix);
}


void Box::DrawScene()
{
	// 후면 버퍼를 지운다.
	D3DImmediateContext->ClearRenderTargetView(RenderTargetView, reinterpret_cast<const float*>(&Colors::LightSteelBlue));

	// 깊이 스텐실 버퍼의 깊이를 1.0f으로 초기화한다.
	D3DImmediateContext->ClearDepthStencilView(DepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	// 입력배치를 입력조립기에 바인드한다.
	D3DImmediateContext->IASetInputLayout(InputLayout);

	// 입력조립의 방식을 삼각형 목록으로 설정한다.
	D3DImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	UINT Stride = sizeof(MyVertex);
	UINT Offset = 0;

	// 정점버퍼를 입력조립기에 바인딩한다.
	D3DImmediateContext->IASetVertexBuffers(0, 1, &BoxVB, &Stride, &Offset);

	// 인덱스버퍼를 입력조립기에 바인딩한다.
	D3DImmediateContext->IASetIndexBuffer(BoxIB, DXGI_FORMAT_R32_UINT, 0);

	XMMATRIX WorldMatrix = XMLoadFloat4x4(&this->WorldMatrix);
	XMMATRIX ViewMatrix = XMLoadFloat4x4(&this->ViewMatrix);
	XMMATRIX ProjectionMatrix = XMLoadFloat4x4(&this->ProjectionMatrix);
	XMMATRIX WorldViewProjMatrix = WorldMatrix * ViewMatrix * ProjectionMatrix;

	FXWorldViewProjMatrix->SetMatrix(reinterpret_cast<float*>(&WorldViewProjMatrix));

	D3DX11_TECHNIQUE_DESC TechDesc;
	Tech->GetDesc(&TechDesc);
	for ( UINT Pass = 0; Pass < TechDesc.Passes; ++Pass )
	{
		// 상수버퍼를 실제로 갱신한다.
		Tech->GetPassByIndex(Pass)->Apply(0, D3DImmediateContext);

		// 렌더링 한다.
		D3DImmediateContext->DrawIndexed(36, 0, 0);
	}

	// 모든 모델을 랜더링했으면 후면버퍼를 스왑한다.
	HR(SwapChain->Present(0, 0));
}


void Box::OnResize()
{
	Direct3DApplication::OnResize();

	// 윈도우가 리사이징되면 종횡비를 다시 갱신하고 새 투영행렬을 만들어야한다.
	XMMATRIX ProjectionMatrix = XMMatrixPerspectiveFovLH(0.25f * MathHelper::PI, GetAspectRatio(), 1.0f, 1000.0f);
	XMStoreFloat4x4(&this->ProjectionMatrix, ProjectionMatrix);
}


void Box::OnMouseDown(WPARAM ButtonState, int x, int y)
{
	LastMousePosition.x = x;
	LastMousePosition.y = y;

	SetCapture(hMainWindow);
}


void Box::OnMouseUp(WPARAM ButtonState, int x, int y)
{
	ReleaseCapture();
}


void Box::OnMouseMove(WPARAM ButtonState, int x, int y)
{
	if ( (ButtonState & MK_LBUTTON) != 0 )
	{
		float DeltaX = XMConvertToRadians(0.25f * static_cast<float>(x - LastMousePosition.x));
		float DeltaY = XMConvertToRadians(0.25f * static_cast<float>(y - LastMousePosition.y));

		Theta += DeltaX;
		Phi += DeltaY;

		Phi = MathHelper::Clamp(Phi, 0.1f, MathHelper::PI - 0.1f);
	}
	else if ( (ButtonState & MK_RBUTTON) != 0 )
	{
		float DeltaX = 0.005f * static_cast<float>(x - LastMousePosition.x);
		float DeltaY = 0.005f * static_cast<float>(y - LastMousePosition.y);

		Radius += DeltaY - DeltaY;

		Radius = MathHelper::Clamp(Radius, 3.0f, 15.0f);
	}

	LastMousePosition.x = x;
	LastMousePosition.y = y;
}


void Box::BuildGeometryBuffers()
{
	// 정점 버퍼 생성

	MyVertex vertices[] =
	{
		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT4((const float*)&Colors::White) },
		{ XMFLOAT3(-1.0f, +1.0f, -1.0f), XMFLOAT4((const float*)&Colors::Black) },
		{ XMFLOAT3(+1.0f, +1.0f, -1.0f), XMFLOAT4((const float*)&Colors::Red) },
		{ XMFLOAT3(+1.0f, -1.0f, -1.0f), XMFLOAT4((const float*)&Colors::Green) },
		{ XMFLOAT3(-1.0f, -1.0f, +1.0f), XMFLOAT4((const float*)&Colors::Blue) },
		{ XMFLOAT3(-1.0f, +1.0f, +1.0f), XMFLOAT4((const float*)&Colors::Yellow) },
		{ XMFLOAT3(+1.0f, +1.0f, +1.0f), XMFLOAT4((const float*)&Colors::Cyan) },
		{ XMFLOAT3(+1.0f, -1.0f, +1.0f), XMFLOAT4((const float*)&Colors::Magenta) },
	};

	D3D11_BUFFER_DESC VertexBufferDesc;
	VertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	VertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	VertexBufferDesc.ByteWidth = sizeof(MyVertex) * 8;
	VertexBufferDesc.CPUAccessFlags = 0;
	VertexBufferDesc.MiscFlags = 0;
	VertexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA VBInitData;
	VBInitData.pSysMem = vertices;

	HR(D3DDevice->CreateBuffer(&VertexBufferDesc, &VBInitData, &BoxVB));

	// 인덱스 버퍼 생성

	UINT Indices[] =
	{
		// 앞면
		0, 1, 2,
		0, 2, 3,

		// 뒷면
		4, 6, 5,
		4, 7, 6,

		// 좌측면
		4, 5, 1,
		4, 1, 0,

		// 우측면
		3, 2, 6,
		3, 6, 7,

		// 윗면
		1, 5, 6,
		1, 6, 2,

		// 아랫면
		4, 0, 3,
		4, 3, 7
	};

	D3D11_BUFFER_DESC IndexBufferDesc;
	IndexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	IndexBufferDesc.ByteWidth = sizeof(UINT) * 36;
	IndexBufferDesc.CPUAccessFlags = 0;
	IndexBufferDesc.MiscFlags = 0;
	IndexBufferDesc.StructureByteStride = 0;
	IndexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;

	D3D11_SUBRESOURCE_DATA IVInitData;
	IVInitData.pSysMem = Indices;

	HR(D3DDevice->CreateBuffer(&IndexBufferDesc, &IVInitData, &BoxIB));
}


void Box::BuildFX()
{
	DWORD ShaderFlags = 0;

#if defined(DEBUG) || defined(_DEBUG)
	ShaderFlags |= D3D10_SHADER_DEBUG;
	ShaderFlags |= D3D10_SHADER_SKIP_OPTIMIZATION;
#endif

	ID3D10Blob* CompiledShader = nullptr;
	ID3D10Blob* CompilationMsgs = nullptr;

	HR(D3DCompileFromFile(L"FX/Color.fx", nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, nullptr, "fx_5_0", ShaderFlags, 0, &CompiledShader, &CompilationMsgs));

	if ( CompilationMsgs != 0 )
	{
		MessageBoxA(0, static_cast<char*>(CompilationMsgs->GetBufferPointer()), 0, 0);
		RELEASECOM(CompilationMsgs);
	}

	HR(D3DX11CreateEffectFromMemory(CompiledShader->GetBufferPointer(), CompiledShader->GetBufferSize(), 0, D3DDevice, &FX));
	RELEASECOM(CompiledShader);

	Tech = FX->GetTechniqueByName("ColorTech");
	FXWorldViewProjMatrix = FX->GetVariableByName("gWorldViewProjMatrix")->AsMatrix();
}


void Box::BuildVertexLayout()
{
	D3D11_INPUT_ELEMENT_DESC VertexDesc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	D3DX11_PASS_DESC PassDesc;
	Tech->GetPassByIndex(0)->GetDesc(&PassDesc);
	HR(D3DDevice->CreateInputLayout(VertexDesc, 2, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &InputLayout));
}


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd)
{
	/* ------------------------------------------------------------------------- */
	// 메모리 누수 체크

#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	Box theApp(hInstance);

	if ( theApp.Initialize() == false )
	{
		return 0;
	}

	return theApp.Run();
}